package com.itheima.Repository;


import com.itheima.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

public interface UserRepository extends JpaRepository<User,Integer> {
    public User findUserByUsernameEquals(String username);
    public List<User> findByUsernameContaining(String username);

    /**
     * 注意不需要增加 save(T)
     */
    @Transactional
    @Modifying
    @Query("update t_user set id=?1,username=?2,password=?3,birthday=?4 where id=?1")
    public void update(Integer id, String username, String password, Date birthday);

    @Transactional
    @Modifying
    @Query("delete from t_user where id=?1")
    public void delete(Integer id);

}
