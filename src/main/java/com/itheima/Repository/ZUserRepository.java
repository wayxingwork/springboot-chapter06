package com.itheima.Repository;

import com.itheima.domain.ZUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

public interface ZUserRepository extends JpaRepository<ZUser,Integer>{
    @Transactional
    @Modifying
    @Query("Update 中文用户表 set 姓名=?2 where id=?1 ")
    public int updateZUser(Integer id,String name);
}
