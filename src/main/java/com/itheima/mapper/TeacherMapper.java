package com.itheima.mapper;


import com.itheima.domain.Teacher;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TeacherMapper {
    @Select("SELECT * FROM teacher WHERE id =#{id}")
    public Teacher findById(Integer id);

    @Select("SELECT * FROM teacher")
    public List<Teacher> getAll();

    @Insert("INSERT INTO teacher(jobno,username,age,mobile) " +
            "values (#{jobno},#{username},#{age},#{mobile})")
    public int addTeacher(Teacher teacher);

    @Update("UPDATE teacher SET username=#{username} WHERE id=#{id}")
    public int updateTeacher(Teacher teacher);

    @Delete("DELETE FROM teacher WHERE id=#{id}")
    public int deleteTeacher(Integer id);
}