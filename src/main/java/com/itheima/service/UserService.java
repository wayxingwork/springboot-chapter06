package com.itheima.service;

import com.itheima.Repository.UserRepository;
import com.itheima.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    public List<User> findAll(){
        return userRepository.findAll();
    }
    public List<User> findByUsername(String username){return userRepository.findByUsernameContaining(username);}

    public List<User> findAll_m(){
        //return (List<User>)this.userRepository.findAll();
        List<User> userList = new ArrayList<User>();
        for (int i=0;i<100;i++){
            userList.add(new User(i,"Tom-模拟"+i,"123",new Date()));
        }
        return userList;
    }

}
