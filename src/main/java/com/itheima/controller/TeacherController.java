package com.itheima.controller;

import com.itheima.domain.Teacher;
import com.itheima.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    @GetMapping("/togetAll")
    public String togetAll(Model model){
        List<Teacher> teacherList = teacherService.getAll();
        model.addAttribute("teacherlist",teacherList);
        return "helloteacher";
    }
}
