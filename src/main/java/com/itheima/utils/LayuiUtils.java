package com.itheima.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LayuiUtils {
    public static Map<String,Object> getDyncTableDataList(Integer page,Integer limit,List listin){
        int total = listin.size();

        // 计算当前需要显示的数据下标起始值
        int startIndex = (page - 1) * limit;
        int endIndex = Math.min(startIndex + limit,total);

        List<Object> pageList = listin.subList(startIndex,endIndex);
        Map<String, Object> resultMap = new HashMap<String, Object>();

        resultMap.put("code", 0);  //此处为0
        resultMap.put("msg", ""); //此处为空
        resultMap.put("count", listin.size());//此处放入总的数据数量
        resultMap.put("data", pageList); //此处应该放入实际页的数据

        return resultMap;
    }
}
