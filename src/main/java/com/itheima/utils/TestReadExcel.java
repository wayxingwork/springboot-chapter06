package com.itheima.utils;

import com.alibaba.excel.EasyExcel;
import com.itheima.domain.DemoData;

public class TestReadExcel {
    public static void main(String[] args) {

        //实现excel读操作
        String filename = "C://temp//write.xlsx";
        EasyExcel.read(filename,DemoData.class,new ExcelListener()).sheet().doRead();
    }
}
