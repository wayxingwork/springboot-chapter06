package com.itheima;

import com.itheima.Repository.UserRepository;
import com.itheima.Repository.ZUserRepository;
import com.itheima.domain.Teacher;
import com.itheima.domain.User;
import com.itheima.domain.ZUser;
import com.itheima.mapper.TeacherMapper;
import com.itheima.service.UserService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.apache.commons.lang3.time.DateUtils.*;

@SpringBootTest
class Chapter06ApplicationTests {

	@Test
	void contextLoads() {
	}
	@Autowired
	private TeacherMapper teacherMapper;
	@Test
	public void testTeacherAdd(){
		Teacher teacher = new Teacher();
		teacher.setJobno("222");
		teacher.setUsername("张大");
		teacherMapper.addTeacher(teacher);
	}
	@Autowired
	private UserRepository userRepository;
	@Test
	public void testUserFindAll(){
		System.out.println(userRepository.findAll());
	}

	@Test
    public void testUserFindBy(){
        System.out.println(userRepository.findUserByUsernameEquals("张三"));
        System.out.println(userRepository.findByUsernameContaining("张"));
    }

	@Test
    public void testUserUpdate(){
	    Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            d  = sdf.parse("2021-1-1");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //User user = new User(1,"张三","123",d);
        this.userRepository.update(1,"张三","pass",d);

    }

	@Test
    public void testUserAdd(){
	    User user=new User(3,"汤姆3","123",new Date());
	    this.userRepository.save(user);

    }
	@Autowired
	private UserService userService;
	@Test
	public void testUserService(){
		System.out.println(userService.findAll());
        System.out.println(userService.findByUsername("张"));
	}

    @Autowired
    private ZUserRepository zUserRepository;
	@Test
	public void testZUser(){
        System.out.println(zUserRepository.findAll());
        System.out.println(zUserRepository.updateZUser(1,"张四"));
    }
}
