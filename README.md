# 第6章 缓存技术 

## 第10周 第一次课程设计
### 目的

体验并比较 模板技术 与 前后端分离开发技术的优缺点

### 准备
- 导入 resources/db 目录下的 chapter05-demo.sql
- 运行 Chapter06Application

### JPA 访问

- UserRepository
- Restful 风格


## 第9周 第一次课程设计

### 准备
- 导入 resources/db 目录下的 chapter05-demo.sql
- 运行 Chapter06Application

### JPA 访问

- CommentRepository
- Restful 风格
- 如何启用缓存
- 如何测试 ab 的使用
